resource "aws_instance" "Terraform_Instance" {
  ami           = var.AMIS[var.AWS_REGION]
  instance_type = "t2.micro"

  tags = {
    Name = "Terraform_Instance"
  }

  provisioner "local-exec" {
        command = "echo ${aws_instance.Terraform_Instance.private_ip} >> private_ips.txt"
    }

  provisioner "file" {
        source = "nginx_script.sh"
        destination = "/tmp/nginx_script.sh"
    }
#    timeouts {
#    create = "15m"
#    }
  provisioner "remote-exec" {
        inline = [
            "chmod +x /tmp/nginx_script.sh",
            "sudo /tmp/nginx_script.sh"
        ]
    }
  connection {
        host        = coalesce(self.public_ip, self.private_ip)
        type        = "ssh"
        user        = var.INSTANCE_USERNAME
        private_key = file(var.PATH_TO_PRIVATE_KEY)
    } 
  # the VPC subnet
  subnet_id = aws_subnet.main-public-1.id

  # the security group
  vpc_security_group_ids = [aws_security_group.allow-ssh.id]

  # the public SSH key
  key_name = aws_key_pair.mykeypair.key_name

  #userdata
  user_data = data.template_cloudinit_config.cloudinit-example.rendered
}
resource "aws_ebs_volume" "ebs-volume-1" {
  availability_zone = "ap-south-1a"
  size              = 20
  type              = "gp2"
  tags = {
    Name = "extra volume data"
  }
}

resource "aws_volume_attachment" "ebs-volume-1-attachment" {
  device_name = var.INSTANCE_DEVICE_NAME
  volume_id   = aws_ebs_volume.ebs-volume-1.id
  instance_id = aws_instance.Terraform_Instance.id
  skip_destroy = true  
}
  output "ip" {
    value = aws_instance.Terraform_Instance.public_ip
} 