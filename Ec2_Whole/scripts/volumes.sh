#!/bin/bash

mkfs.ext4 /dev/xvdh
mkdir -p /data
echo '/dev/xvdh /data ext4 defaults 0 0' >> /etc/fstab
mount /data

# install docker
curl https://get.docker.com | bash